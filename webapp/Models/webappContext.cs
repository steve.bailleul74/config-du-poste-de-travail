﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapp.Models
{
    public class webappContext : DbContext
    {

        public webappContext()
        {
            
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"server=localhost; database=webappdb; user id=monsite1; password=monsite1; ");
            
        }

        public virtual DbSet<Person> Persons { get; set; }
    }
}
